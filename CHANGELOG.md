# Changelog

## 0.0.3

- Suppression du lancement automatique du flatpak session help (il faut  maintenant passer un argument au script bash pour le lancer), correction : le lancement du session helper se fait dans un terminal gnome

- Correction de la description d'un problème avec les extensions live server

- Mise à jour du script vscode.bash, un fichier manquait à l'appel pour ajouter Visual Studio Code comme entrée dans le launcher d'unity, il est maintenant directement écrit depuis le fichier bash, il n'est donc plus necessaire

- Mise à jour d'une capture d'écran

## 0.0.2

- Petit patch pour le problème de couleurs avec flatpal

## 0.0.1

- Première commit
- Création du tutoriel