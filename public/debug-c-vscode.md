# Deboguage des programmes C/C++ avec les outils de deboguage microsoft sur les PCs de l'IUT

- [Deboguage des programmes C/C++ avec les outils de deboguage microsoft sur les PCs de l'IUT](#deboguage-des-programmes-cc-avec-les-outils-de-deboguage-microsoft-sur-les-pcs-de-liut)
  - [Introduction & problème](#introduction--probl%C3%A8me)
  - [Choses nécessaires](#choses-n%C3%A9cessaires)
  - [Installation rapide](#installation-rapide)

![Debug hero](https://code.visualstudio.com/assets/docs/editor/debugging/debugging_hero.png)

## Introduction & problème

L'un des problèmes avec l'extension C/C++ de Microsoft est qu'il n'est pas possible de déboguer un programme C/C++ losque vscode s'execute dans une sandbox.

Ce tutoriel est l'une des solutions pour pouvoir déboguer du C/C++ sur vscode et profiter pleinement des foncionnalités de déboguage proposées (pas à pas, point d'arrêts, inspection de variable...)

## Choses nécessaires

- Avoir mit en place le [script bash vscode.bash, voir ici](vscode-fix.md)

- Avoir téléchargé le dossier [.vscode](https://gitlab.com/maxerbox/awesome-iut-nantes/tree/master/.vscode), disponible dans cette repo

## Installation rapide

Sur les ordinateurs de l'IUT:

1. Déplacez le dossier .vscode dans votre projet actuel

2. Compilez votre projet avec cette ligne de commande

   - `gcc -o exec -Wall -Wextra -pedantic -g fichier.c`
     - L'option -o exec permet de spéficier le fichier de sortit (exec est utilisé dans le fichier `tasks.json`)
     - L'option -g permet d'activer la compilation du programme avec les outils de déboguage.

3. Enfin, démarrez le déboguage, en allant dans l'onget déboguage de vscode, puis en cliquant sur le "Play" en haut à gauche (avec la config `(gdb) Launch` selectionnée)

   - Un terminal gnome devrait s'ouvrir, attendez un peu
   - Le programme devrait s'executer normalement.
   - N'hésitez pas à ajouter des points d'arrêts !
