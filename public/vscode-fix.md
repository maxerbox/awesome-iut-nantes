# Installer des extensions sur Visual Studio Code à l'IUT de Nantes

## Introduction

Ce script permet d'**installer des extensions Visual Studio Code (aka Code-OSS)** sur **les ordinateurs de l'IUT de Nantes**. Les extensions et les paramètres de code seront sauvegardées sur le serveur

## Les plus

- Ajoutez des tonnes d'extensions

- Déboguez vos programmes à l'IUT

### A voir aussi

- [Deboguage des programmes C/C++ avec les outils de deboguage microsoft sur les PCs de l'IUT](debug-c-vscode.md)

## Fonctionnalités

- Dit à vscode quels proxies utiliser.

- Création d'une entrée bureau: Taper `Visual Studio Code` dans la bar de recherche `d'Unity` et cliquer sur l'icöne ouvrira Visual Studio Code de la bonne façon.

- Ajout d'un alias: Tapez `code` dans un terminal et vscode s'ouvrira

- Grâce à l'entrée bureau: Possibilité de mettre vscode dans la bar des favoris, après avoir executer le script, et visual studio code s'ouvrira de la bonne façon.

- Pas besoin de télécharger vscode portable: Sauvergarde des extensions et des settings sur le serveur (dans `~/Documents/.code` & `~/Documents/.code-exts`)

- Lance flatpak avec les bons arguments (car Code - OSS tourne dans la sandbox flatpak):

  - VSCODE a accès à quasiment toute la machine hôte.

  - VSCODE a accès au dbus système : il peut utiliser la commande [`flatpak-spawn --host` pour executer une commande sur la machine hôte](#flatpak-spawn)

- Fix de flatpak-spawn : remplacement du deamon dbus avec flatpak-help à chaque démarrage, permet d'avoir les couleurs sur son terminal

## Choses nécessaires

- Télécharger le [script bash vscode.bash](https://gitlab.com/maxerbox/awesome-iut-nantes/blob/master/vscode.bash)
- Télécharger le [raccourci bureau (optionnel) vscode.desktop](https://gitlab.com/maxerbox/awesome-iut-nantes/blob/master/vscode-launcher.desktop)

## Installation rapide

Sur les ordinateurs de l'IUT:

1. Déplacez le script vscode.bash dans votre dossier `Documents` (celui qui est en vert, qui est monté sur le réseau)

2. Après l'avoir déplacé, si vous avez téléchargé **le raccourci bureau** `vscode-launcher.desktop`:

   - déplacez le dans votre Dossier `Bureau` (celui qui est en vert, qui est monté sur le réseau).

   - ouvrez le avec votre éditeur favoris et remplacer `*********` avec votre numéro étudiant, au niveau de cette ligne: `Exec=bash /home/*********/Documents/vscode.bash`

3. Enfin, exécutez Visual Studio Code (aka Code-OSS) comme ceci:

   - si vous n'avez pas installé le raccourci, ouvrez un terminal dans votre dossier `Documents` et faites `./vscode.bash`

   - sinon, double-cliquez simplement sur le raccourci disponible sur votre bureau, et cliquez sur `Faire confiance et lancer`.

   - Une fois le script exécuté pour la première fois, vous pourrez passer par la recherche du launcher `Unity`:
   ![Unity](ex1.png)

   - Si vous ajoutez visual studio code dans les favoris, vous pourrez le lancer depuis le dash gnome.

   - tapez `code` dans un terminal pour ouvrir vscode

    **PS: Il vous faudra recliquer sur le raccourci/exécuter le script à chaque changement de pc, ou après 2 semaines.**

4. Paramétrez vscode

    - [Terminal hôte en tant que terminal intégré](#terminal-hote) (recommandé): permet d'avoir les commandes hôtes dans le terminal intégré de vscode

## flatpak-spawn

### C'est quoi

Selon flatpak:
>flatpak-spawn est un nouvel outil permettant d'exéctuer des commandes "hotes" (si il y a permission) et de créer des sandboxs à partir d'une sandbox (utilise l'api "Portail").

`flatpak-spawn` permet d'éxecuter des commandes depuis vscode sur la machine hôte: en effet, Code-OSS tourne dans une sandbox (environnement restreint) nommée "flatpak".
L'utilité de `flatpak-spawn` est de contourner les restrictions de la sandbox et d'accéder aux commandes non disponibles depuis le terminal intégré, comme `gnome-terminal`, `man`...

### Comment l'utiliser

`flatpak-spawn --host <commande> [arg1, arg2, arg3...]`

Ex: lancer un terminal bash depuis le terminal intégré de vscode : `flatpak-spawn --host gnome-terminal -- bash`

## Mise à jour de Code - OSS sur les PCs de l'IUT

Pour mettre à jour Code- OSS, il suffit de faire cette commande (les droits super utilisateur ne sont pas nécessaires) :

```bash
flatpak update com.visualstudio.code.oss
```

Utilisez ceete commande sur toutes les machines possibles !

## terminal-hote

Permet d'avoir vos commandes préferrées dans le terminal de vscode, comme `man`, `gnome-terminal` etc...

### Paramétrage

Pour accéder au terminal hôte (en gros le terminal intégré sera une instance de bash sur l'hôte), il vous faut :

- Ouvrir les paramètres de code :

![Paramètre](ex2.png)

- Aller dans l'editeur json des paramètres (si votre version de vscode est à jour) en cliquant sur les `{}` en haut à droite

![JsonParamètre](ex3.png)

- Rechercher dans la bar `terminal.integrated.shell.linux` et cliquer sur le crayon (soyez sur d'être dans les paramètres utilisateurs):

![Paramètre Intégré Shell](ex4.png)

- Remplacez ce qu'il y a entre guillement par `"flatpak-spawn",`

- Ensuites copier coller ceci en dessous :

```json
"terminal.integrated.shellArgs.linux": [
    "--host",
    "bash"
]
```

- Et enfin sauvegarder les paramètres (Ctrl + S). Vérifiez en ouvrant un nouveau terminal (Terminal > New terminal)

    Normalement vous devriez obtenir ceci:
    ![Shell](ex6.png)
    et pouvoir ouvrir un terminal gnome depuis le terminal intégré:
    ![Gnome-terminal](ex7.png)

- Quelques problèmes peuvent apparaitre, voir la liste ci dessus:

### Problème : Ioctl() innaproprié

Apparamment, cela vient du fait que le pseudo terminal de vscode est déja un terminal de contrôle d'un groupe de session différent que celui de bash. [Cela provient de flatpak](https://github.com/flatpak/flatpak/blob/64e67641376a6b38acef95bc1bc4d803e20fdf59/session-helper/flatpak-session-helper.c#L187)

Si vous savez comment régler ce problème, dites le moi !

#### Pas de couleurs dans le terminal

Si vous souhaitez obtenir des couleurs dans le terminal intégré de vscode modifié, lancez le script bash avec comme argument: couleur.
Ex: `./vscode.bash couleur`

## Problèmes connues

### Problème avec le raccourci en favoris

Le favoris s'affichera même si vous n'avez pas exécuter le script sur un nouveau PC (après changement).
Vérifiez bien que le favoris affiche "visual Studio Code Edited" lorsque vous passez la souris dessus

### Problèmes sur certaines extensions

Il y a quelques problèmes sur certaines extensions :

####  live preview html

Problème: page erreur du proxy dans la side preview

Les extensions qui font des sides live previews dans vscode afin de faire un rendu html en direct et qui utilisent un serveur web ne fonctionnent pas : vscode essait d'ouvrir la side preview sur le proxy  de l'IUT qui est en localhost, le proxy affiche une page web "adresse non résolue"

#### vscode-icons

Vscode icons a du mal à sauvegarder ses paramètres quand les répertoires de configs et d'extensions sont spécifié en paramètre (avec `--user-data-dir`).

#### Settings Sync

Settings sync ne peut pas fonctionner quand les répertoires de configs et d'extensions sont spécifié en paramètre (avec `--user-data-dir`).

### globalState non sauvegardé

Problème : les extensions affichent "Extension mise à jour" dans une notification ou dans un nouvel onglet.

Vscode a un problème tel que si jamais les répertoires de configs et d'extensions sont spécifiés en paramètre (avec `--user-data-dir`) le globalState de l'extension n'est pas propremet sauvegardé

Solution : Si l'extension propose dans ses paramètres de désactiver la notification lors d'une mise à jour de l'extension

### Script non permanent

Et oui, l'un des problèmes les plus récurrents et le fait qu'il faut obligatoirement ré-executer le script si jamais vous changez de PC, ou si vous êtes plus de 2 semaines sur un même PC.

Une solution: le raccourci bureau, pour vous faciliter la vie. Je recherche encore un moyen de lancer une entrée bureau au démarrage, mais malheureusement le dossier dans `~/.config/autostart` n'est pas monté sur le réseau.
Seul la configuration dans `~/.config/dconf` est synchronisée, alors si jamais vous trouvez un paramètre gnome permettant d'éxécuter un script au démarrage, dite le moi !

### VSCODE ne garde pas mes dossiers ouverts récemments

L'une des fonctionnalités de vscode est de gardé en mémoire les dossiers ouverts récemments.
Il semble que lorsque ont spécifie un répertoire spéficiques pour la config et les extensions, vscode "oublit" les dossiers.
Un conseil: [installer Project Manager](#Project-manager).

## Listes d'extensions & paramètres sympas

### Window custom

Mettez le paramètre `"window.titleBarStyle": "custom"` afin d'avoir la fenêtre custom de vscode (plus belle)

### Project-manager

Name: Project Manager
Id: alefragnani.project-manager
Description: Easily switch between projects
Version: 10.3.0
Publisher: Alessandro Fragnani
[VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager)

Permet de sauvegarder ses projets dans une liste de projets. Ne pas oublier de changer le paramètre `projectManager.projectsLocation` avec un chemin menant sur un fichier js sur le serveur.

### Microsoft cpp tools

Name: C/C++
Id: ms-vscode.cpptools
Description: C/C++ IntelliSense, debugging, and code browsing.
Version: 0.20.1
Publisher: Microsoft
[VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)

Permet de debogguer le C

### Vscode icons

Name: vscode-icons
Id: robertohuertasm.vscode-icons
Description: Icons for Visual Studio Code
Version: 8.0.0
Publisher: Roberto Huertas
[VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons)

Des icones un peu mieux !

### Rainbow brackets

Name: Rainbow Brackets
Id: 2gua.rainbow-brackets
Description: A rainbow brackets extension for VS Code.
Version: 0.0.6
Publisher: 2gua
[VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=2gua.rainbow-brackets)

Pour voir ses parenthèses

### vscode-pdf

[https://marketplace.visualstudio.com/items?itemName=tomoki1207.pdf](https://marketplace.visualstudio.com/items?itemName=tomoki1207.pdf)

Affiche les pdf dans vscode

### W3C Validation

[https://marketplace.visualstudio.com/items?itemName=Umoxfo.vscode-w3cvalidation](https://marketplace.visualstudio.com/items?itemName=Umoxfo.vscode-w3cvalidation)

Validation selon le format W3C du code html

### Langue francaise

[https://marketplace.visualstudio.com/items?itemName=MS-CEINTL.vscode-language-pack-fr](https://marketplace.visualstudio.com/items?itemName=MS-CEINTL.vscode-language-pack-fr)

Vscode en français

### Gitlens

Name: GitLens — Git supercharged
Id: eamodio.gitlens
Description: Supercharge the Git capabilities built into Visual Studio Code — Visualize code authorship at a glance via Git blame annotations and code lens, seamlessly navigate and explore Git repositories, gain valuable insights via powerful comparison commands, and so much more
Version: 9.4.1
Publisher: Eric Amodio
[VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)

Implémentation des gits plus évoluée

Etc...