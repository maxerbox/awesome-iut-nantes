# Choses utiles sur les PCs de l'IUT

- [Installer des extensions sur Visual Studio Code à l'IUT de Nantes](vscode-fix.md)
- [Deboguage des programmes C/C++ avec les outils de deboguage microsoft sur les PCs de l'IUT](debug-c-vscode.md)
