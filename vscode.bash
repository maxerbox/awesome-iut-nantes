#!/bin/bash

#Ca pourrai être mieux fait
if [  -f ~/.local/share/applications/com.visualstudio.code.oss.desktop ]; then
    notify-send  "Just pass through the launcher, type Visual Studio Code"
fi
if [ ! -f ~/.bash_aliases ]; then
    echo "alias code=\"/usr/bin/flatpak --socket=session-bus --socket=system-bus run com.visualstudio.code.oss --proxy-server=\"http=$http_proxy;https=$https_proxy\" --new-window --user-data-dir=\"$HOME/Documents/.code\" --extensions-dir=\"$HOME/Documents/.code-exts\"  --branch=stable --arch=x86_64   --unity-launch\"" >  ~/.bash_aliases
    notify-send  "Vscode launcher" "Created .bash_aliases and setting it to \"code\" for vscode"
else
    if ! grep -q "alias code=\"/usr/bin/flatpak --socket=session-bus --socket=system-bus run com.visualstudio.code.oss --proxy-server=\"http=$http_proxy;https=$https_proxy\" --new-window --user-data-dir=\"$HOME/Documents/.code\" --extensions-dir=\"$HOME/Documents/.code-exts\"  --branch=stable --arch=x86_64   --unity-launch\"" ~/.bash_aliases; then
        echo "alias code=\"/usr/bin/flatpak --socket=session-bus --socket=system-bus run com.visualstudio.code.oss --proxy-server=\"http=$http_proxy;https=$https_proxy\" --new-window --user-data-dir=\"$HOME/Documents/.code\" --extensions-dir=\"$HOME/Documents/.code-exts\"  --branch=stable --arch=x86_64   --unity-launch\"" >>  ~/.bash_aliases
        notify-send  "Appended \"code\" for vscode to ~/.bash_aliases"
    fi
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
if [ ! -f ~/.local/share/applications/com.visualstudio.code.oss.desktop  ]; then
    printf '[Desktop Entry]\nName=Visual Studio Code Edited\nComment=Code Editing. Redefined.\nGenericName=Text Editor\nExec=$TOEDIT\nIcon=com.visualstudio.code.oss\nType=Application\nStartupNotify=true\nStartupWMClass=code - oss\nCategories=Utility;TextEditor;Development;IDE;\nMimeType=text/plain;inode/directory;\nActions=new-empty-window;\nKeywords=vscode;\nX-Flatpak=com.visualstudio.code.oss\nName[fr_FR]=Visual Studio Code Edited' | sed --expression="s#\$TOEDIT#/usr/bin/flatpak --socket=session-bus --socket=system-bus run com.visualstudio.code.oss --proxy-server=http=$http_proxy;https=$https_proxy --new-window --user-data-dir=$HOME/Documents/.code --extensions-dir=$HOME/Documents/.code-exts  --branch=stable --arch=x86_64   --unity-launch #g" | tee ~/.local/share/applications/com.visualstudio.code.oss.desktop
    notify-send  "Vscode is now ready in the search bar. Just type Visual Studio Code"
fi

# small fix for hosted integrated terminal colors

[ ! -z "$1" ] && mkdir -p ~/.config/autostart
[ ! -z "$1" ] && printf "[Desktop Entry]\nType=Application\nVersion=1.0\nName=flatpak-session-help\nExec=gnome-terminal -- /usr/lib/flatpak/flatpak-session-helper -r -v\nStartupNotify=false" > ~/.config/autostart/flatpak-session-help.desktop
[ ! -z "$1" ] && gnome-terminal -- /usr/lib/flatpak/flatpak-session-helper -r -v
[ ! -z "$1" ] && notify-send  "Started & Fixed flatpak-session, now launching..."

/usr/bin/flatpak --socket=session-bus --socket=system-bus --filesystem=host run com.visualstudio.code.oss --proxy-server="http=$http_proxy;https=$https_proxy" --new-window --user-data-dir="$HOME/Documents/.code" --extensions-dir="$HOME/Documents/.code-exts"  --branch=stable --arch=x86_64   --unity-launch

